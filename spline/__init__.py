import spline_extension
import numpy as np

class SplineInterpolator:
    def __init__(self, C, order=[], periodic=[]):
        self.C = C
        self.order = order
        self.periodic = periodic

    def __call__(self, x):
        return spline_extension.spline_grid(self.C,x,order=self.order,periodic=self.periodic)

    @property
    def dx(self):
        class D:
            def __getitem__(subself,diff):
                if isinstance(diff, int):
                    diff = (diff,)
                def dummy(x):
                    return spline_extension.spline_grid(self.C,x,order=self.order,periodic=self.periodic,diff=diff)

                return dummy
        return D()