from distutils.core import setup, Extension
import numpy as np


spline_module = Extension('spline_extension',
                     sources = ['src/extension.cpp'],
                     extra_compile_args=['-std=c++11'],
                     include_dirs = [np.get_include()],
                     )

setup (name = 'Spline',
       version = '1.0',
       packages=['spline'],
       description = 'B-Spline interpolation library',
       ext_modules = [spline_module])
