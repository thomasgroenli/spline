#undef Py_LIMITED_API

#include <Python.h>
#include <thread>
#include <vector>
#include <numpy/arrayobject.h>

//CPU specialization of actual computation.
double kernel_cpu(double x, int p, int diff, double *tmp) {
	if (diff > p) {
		return 0;
	}
	x += (p + 1) / 2.;
	int k = (int)x;
	for (int i = 0; i < p + 1; i++) {
		tmp[i] = k == i;
	}

	for (int i = 0; i < p; i++) {
		for (int j = 0; j < p - i; j++) {
			tmp[j] = i < p - diff ? (x - j) / (i + 1)*tmp[j] + (i + 2 + j - x) / (i + 1)*tmp[j + 1] : tmp[j] - tmp[j + 1];
		}
	}
	return tmp[0];
}

void spline_grid_kernel_cpu(int start, int stop, int n_dims, int n_channels, const int *grid_dim, const int *strides, const int *order, const int *diff, const int *periodic, const double *positions, const double *coefficients, double *out) {
	int n_neigh = 1;
	for (int i = 0; i < n_dims; i++) {
		n_neigh *= order[i] + 1;
	}

	int max_order = 0;
	for (int i = 0; i < n_dims; i++) {
		max_order = max_order > order[i] ? max_order : order[i];
	}
	
	int *idx = new int[n_dims];
	double *shift = new double[n_dims];
	double *kernel_tmp = new double[max_order + 1];

	for (int i = start; i < stop; i++) {
		bool valid = true;
		for (int j = 0; j < n_dims; j++) {
			double tmp = positions[i*n_dims + j];
			
			if (periodic[j]) {
				tmp = fmod(tmp,1) + (tmp < 0);
			}
			valid &= (0 <= tmp && tmp <= 1);
			shift[j] = modf(tmp*(grid_dim[j] + periodic[j] - 1) + 0.5, &tmp) - 0.5;
			idx[j] = (int)tmp;
		}
		for (int j = 0; j < n_channels; j++) {
			out[i*n_channels + j] = valid ? 0 : NAN;
		}

		for (int j = 0; j < n_neigh; j++) {
			int reduce = j;
			int flat = 0;
			double Wij = 1;
			for (int k = n_dims - 1; k >= 0; k--) {
				int offset = -(order[k] + 1 - int(shift[k] + 1)) / 2 + (reduce % (order[k] + 1));
				flat += (int) (strides[k] *(periodic[k] ? ((idx[k] + offset + grid_dim[k]) % grid_dim[k]) : fmin(fmax(idx[k] + offset, 0), grid_dim[k] - 1)));
				Wij *= kernel_cpu(shift[k] - offset, order[k], diff[k], kernel_tmp)*pow(grid_dim[k]+periodic[k]-1, double(diff[k]));
				reduce /= order[k] + 1;
			}
			for (int k = 0; k < n_channels; k++) {
				if (valid) out[i*n_channels + k] += Wij * coefficients[n_channels*flat + k];
			}
		}
	}
	delete[] idx;
	delete[] shift;
	delete[] kernel_tmp;
}


int fill_from_sequence(int *target, PyObject *seq, int N, int default_value, const char* arg) {
	for (int i = 0; i < N; i++) {
		target[i] = default_value;
	}
	if (seq != NULL) {
		if (PySequence_Check(seq)) {
			for (int i = 0; i < PySequence_Size(seq); i++) {
				PyObject *item = PySequence_GetItem(seq, i);
				if (PyLong_Check(item)) {
					target[i] = PyLong_AsLong(item);
				}
				else {
					PyErr_SetString(PyExc_ValueError, arg);
					return 1;
				}
			}
		}
		else {
			PyErr_SetString(PyExc_ValueError, arg);
			return 1;
		}
	}
	return 0;
}


PyObject* spline_grid(PyObject *, PyObject *args, PyObject *kwargs) {

	PyObject *C_arg=NULL, *x_arg=NULL;
	PyArrayObject *C_arr=NULL, *x_arr=NULL;

	PyObject *py_order=NULL, *py_diff=NULL, *py_periodic=NULL;

	static char *kwlist[] = { "C", "x", "order", "diff", "periodic", NULL };

	if (!PyArg_ParseTupleAndKeywords(args, kwargs, "OO|OOO", kwlist,
									&C_arg, &x_arg,
									&py_order, 
									&py_diff, 
									&py_periodic
									)) return NULL;


	C_arr = (PyArrayObject*)PyArray_FROM_OF(C_arg, NPY_IN_ARRAY);
	x_arr = (PyArrayObject*)PyArray_FROM_OF(x_arg, NPY_IN_ARRAY);
	if(PyArray_TYPE(C_arr) != NPY_DOUBLE || PyArray_TYPE(x_arr) != NPY_DOUBLE) {
		PyErr_SetString(PyExc_ValueError, "Input arguments must be double precision");
		return NULL;
	}

	int n_dims = (int)PyArray_NDIM(C_arr)-1;
	int *shape = new int[n_dims];
	int *strides = new int[n_dims];

	int n_channels = (int)(PyArray_SHAPE(C_arr)[n_dims]);
	int N = (int)PyArray_SIZE(x_arr)/n_dims;

	for(int i=0; i<n_dims; i++) {
		shape[i] = (int)(PyArray_SHAPE(C_arr)[i]);
		strides[i] = (int)(PyArray_STRIDES(C_arr)[i]/PyArray_ITEMSIZE(C_arr)/n_channels);
	}

	int *order = new int[n_dims];
	int *diff = new int[n_dims];
	int *periodic = new int[n_dims];

	if (fill_from_sequence(order, py_order, n_dims, 1, "order")) return NULL;
	if (fill_from_sequence(diff, py_diff, n_dims, 0, "diff")) return NULL;
	if (fill_from_sequence(periodic, py_periodic, n_dims, 0, "periodic")) return NULL;

	int out_dims = (int)PyArray_NDIM(x_arr);
	long long *out_shape = new long long[out_dims];
	for(int i=0; i<out_dims-1; i++) {
		out_shape[i] = PyArray_SHAPE(x_arr)[i];
	}
	out_shape[out_dims-1] = n_channels;
	
	PyObject* out_arr = PyArray_SimpleNew(out_dims, (npy_intp*)out_shape, NPY_DOUBLE);
	
	int n_threads = std::thread::hardware_concurrency();
	if(n_threads == 0) {
		n_threads = 1;
	}
	int block_size = N/n_threads+bool(N%n_threads);

	std::vector<std::thread> threads;
	for(int i=0; i<n_threads; i++) {
		int start = i*block_size;
		int end  = (int)fmin((i+1)*block_size, N);

		threads.push_back(std::thread(spline_grid_kernel_cpu, start, end, n_dims, n_channels, shape, strides, order, diff, periodic, (double*)PyArray_DATA(x_arr), (double*)PyArray_DATA(C_arr), (double*)PyArray_DATA(out_arr)));
	}
	for(int i=0; i<n_threads; i++) {
		threads[i].join();
	}

	delete[] shape;
	delete[] strides;
	delete[] order;
	delete[] diff;
	delete[] periodic;
	delete[] out_shape;
	
	Py_DECREF(C_arr);
	Py_DECREF(x_arr);

	return out_arr;
}

static PyMethodDef spline_methods[] = {
	{ "spline_grid", (PyCFunction)spline_grid, METH_VARARGS | METH_KEYWORDS, "Spline grid interpolation function" },
	{ NULL, NULL, 0, NULL }
};

static PyModuleDef spline_module = {
	PyModuleDef_HEAD_INIT,
	"spline",
	"B-Spline interpolation library",
	0,
	spline_methods,
	NULL,
	NULL,
	NULL,
	NULL
};

PyMODINIT_FUNC PyInit_spline_extension() {
	import_array();
	return PyModule_Create(&spline_module);
}
